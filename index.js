import data from "./data.json" assert { type: "json" };
import ics from "https://cdn.skypack.dev/ics@2?dts";
import { DateTime } from "https://cdn.skypack.dev/luxon@3?dts";

function createICS(dates, startDate = DateTime.fromISO("1900-01-01")) {
  const { error, value } = ics.createEvents(
    // See: https://www.npmjs.com/package/ics
    dates.flatMap((dateString) => {
      const date = DateTime.fromISO(dateString);
      if (date < startDate) {
        return [];
      }
      const end = date.plus(1, "day");
      return {
        start: [date.get("year"), date.get("month"), date.get("day")],
        startInputType: "local",
        end: [end.get("year"), end.get("month"), end.get("day")],
        title: `Family and Friends Day (${
          date.toLocaleString({ month: "long", year: "numeric" })
        })`,
        description:
          "See: https://about.gitlab.com/company/family-and-friends-day/ for more details",
        categories: [],
        status: "CONFIRMED",
        busyStatus: "BUSY",
        // organizer: {  },
        // attendees: []
      };
    }),
  );
  if (error) {
    console.error(error);
    throw new Error("Could not ");
  }
  return value;
}

await Deno.mkdir("./public/ics", { recursive: true });
await Deno.writeTextFile("./public/ics/all_ff_days.ics", createICS(data.days));
await Deno.writeTextFile(
  "./public/ics/future_ff_days.ics",
  createICS(data.days, DateTime.now()),
);
