---
title: "Family and Friends Day Calendar"
author: [Lukas Eipert]
date: "2023-01-23"
...

This is a simple page which provides GitLab Family and Friends days as a calendar.

Use one of the calendar calendar to show/import them in your private calendars.

- [All Family and Friends days](https://leipert-projects.gitlab.io/family-and-friends-day-ics/ics/all_ff_days.ics)
- [Future Family and Friends days](https://leipert-projects.gitlab.io/family-and-friends-day-ics/ics/future_ff_days.ics)

Data taken from [the handbook](https://about.gitlab.com/company/family-and-friends-day/).

[Contributions welcome](https://gitlab.com/leipert-projects/family-and-friends-day-ics)
